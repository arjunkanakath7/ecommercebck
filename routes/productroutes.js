const express = require('express')
const { getallproducts, getproductbyid, addnewproduct, updateproduct, deleteproduct } = require('../controllers/productcoontroller')
const router = express.Router()

//1. Get all products
router.get('/', getallproducts)
  
  //2. Get a product by ID
  router.get('/:productID', getproductbyid)
  
  //3. Add a new product
  router.post('/', addnewproduct)
  
  //4. Update a product
  router.patch('/:productID',updateproduct )  
  
  //5. Delete a product
  router.delete('/:productID', deleteproduct)

module.exports = router