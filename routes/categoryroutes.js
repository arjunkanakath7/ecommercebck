const express = require('express')
const { getallcategory, getcategorybyid, addnewcategory, updatecategory, deletecategory } = require('../controllers/categorycontroller')
const router = express.Router()

 //1. Get all categories
 router.get('/', getallcategory)  

  //2. Get  a category by ID
  router.get('/:categoryID', getcategorybyid)

  //3. Add a new category
  router.post('/', addnewcategory)

  //4. Update a category
  router.patch('/:categoryID',updatecategory )

  //5. Delete a category
  router.delete('/:categoryID', deletecategory)

module.exports = router