const Product = require("../model/productmodel");

const getallproducts =async(req, res) => {
  const products = await Product.find({});
  res.json(products)
  }
 const getproductbyid = async(req, res) => {
  try{
    const product =  await Product.findById(req.params.productID).exec();
    res.status(200).json(product)
  }
   catch(error){
    res.status(404).send("product not found")
   }
  }
  const addnewproduct = (req, res) => {
    res.send('Not written')
  }
  const updateproduct = (req, res) => {
    res.send('Not written')
  }
  const deleteproduct = (req, res) => {
    res.send('Not written')
  }
  module.exports = {
    getallproducts,getproductbyid,addnewproduct,updateproduct,deleteproduct
  }