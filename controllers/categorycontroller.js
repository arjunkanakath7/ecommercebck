const Category = require("../model/categorymodel");

const getallcategory =async(req, res) => {
    const categories = await Category.find({});
    res.json(categories)
  }

 const  getcategorybyid = async(req, res) => {
  try {
    const category = await Category.findById(req.params.categoryID).exec();
    res.status(200).json(category)  
  }
  catch (error) {
    res.status(404).send('Category Not found')
  }
}

  const addnewcategory = (req, res) => {
   console.log(req.body)
   res.send('Not written')
  }

  const updatecategory = (req, res) => {
    res.send('Not written')
  }

  const deletecategory = (req, res) => {
    res.send('Not written')
  }


module.exports = {
    getallcategory,getcategorybyid,addnewcategory,updatecategory,deletecategory
}  